export interface UseCase {
  request: {
    input: string[];
    output: string;
  };
}
